# README #

### What is this repository for? ###

* Materials for the first session of CSS Fundamentals - Intro to CSS and Selectors


### How do I get set up? ###

* Clone down repo
* Open [CodePen.io](https://codepen.io/pen/)
* Paste contents of index.html into the HTML Pane
* Paste contents of styles.css into the CSS Pane


### Who do I talk to? ###

* [Gena Marais](mailto:gena@psybergate.co.za)
* [Travis Somaroo](mailto:travis@psybergate.co.za)